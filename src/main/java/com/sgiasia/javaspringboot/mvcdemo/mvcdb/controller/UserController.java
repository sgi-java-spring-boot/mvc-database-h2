package com.sgiasia.javaspringboot.mvcdemo.mvcdb.controller;

import com.sgiasia.javaspringboot.mvcdemo.mvcdb.model.UserTable;
import com.sgiasia.javaspringboot.mvcdemo.mvcdb.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
public class UserController {
    private final UserRepository userRepository;
    private final Logger LOG = LoggerFactory.getLogger("Application");

    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping("/showusers")
    public List<UserTable> showUsers(Model model){
        List<UserTable> users = this.userRepository.findAll();
        LOG.info("User found {}", users);
        return users;
    }
}
