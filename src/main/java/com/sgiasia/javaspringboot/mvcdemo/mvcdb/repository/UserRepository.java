package com.sgiasia.javaspringboot.mvcdemo.mvcdb.repository;

import com.sgiasia.javaspringboot.mvcdemo.mvcdb.model.UserTable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserTable, Long> { }
