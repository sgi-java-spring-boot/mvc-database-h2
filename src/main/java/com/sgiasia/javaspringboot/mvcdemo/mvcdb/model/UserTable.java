package com.sgiasia.javaspringboot.mvcdemo.mvcdb.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class UserTable {

    @Id
    private Long id;
    private String name;
    private int age;

    public UserTable(){

    }

    public UserTable(Long id, String name, int age){
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public String toString(){
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}

