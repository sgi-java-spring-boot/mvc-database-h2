package com.sgiasia.javaspringboot.mvcdemo.mvcdb;

import com.sgiasia.javaspringboot.mvcdemo.mvcdb.model.UserTable;
import com.sgiasia.javaspringboot.mvcdemo.mvcdb.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MvcdbApplication implements CommandLineRunner {
	private final Logger LOG = LoggerFactory.getLogger("Application");
	private final UserRepository userRepository;

	public MvcdbApplication(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	public static void main(String[] args) {
		SpringApplication.run(MvcdbApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		UserTable user1 = new UserTable(1L, "Sergey", 24);
		UserTable user2 = new UserTable(2L, "Ivan", 26);
		UserTable user3 = new UserTable(3L, "Adam", 31);

		LOG.info("Inserting data in DB.");
		userRepository.save(user1);
		userRepository.save(user2);
		userRepository.save(user3);

		LOG.info("User count in DB: {}", userRepository.count());

		LOG.info("User with ID 1: {}", userRepository.findById(3L));

		LOG.info("Deleting user with ID 2L form DB.");
		userRepository.deleteById(2L);
		LOG.info("User count in DB: {}", userRepository.count());
	}
}
